
// Mostrar en pantallas
function writeScreen(value){

    var entrada = $("#entrada").val();

    //Si esta vacio imprime
    if(entrada == ''){
        $("#entrada").val(value);
    
    // "Y Si" value es igual a "0" y distinto a "." se corta el flujo con "return false"
    }else if(entrada == '0' && value != '.'){ 
    return false; //Solo permite un cero al inicio

    //si value es igual a "." y entrada contiene un punto, corta el flujo
    } else if (value == '.' && entrada.includes('.')) {

        return false;
    
    /*Si el campo entrada ya contiene un simbolo monetario, borrar el simbolo 
    y concadenar el siguiente numero introducido, para conseguir mayor fluidez*/
    }else if(entrada.includes('€') || entrada.includes('$') || entrada.includes('£') || entrada.includes('¥')){

        entrada = entrada.slice(0,-1);
        entrada += value; //concadena el numero anterior
        $("#entrada").val(entrada); //mostrara los numeros en la pantalla
    
    }else{
        //Si no esta vacio concadena
        entrada += value; //concadena el numero anterior
        $("#entrada").val(entrada); //mostrara los numeros en la pantalla
    }
    
}

//Borrar todo
$(document).on("click","#c",function(event){

    $("#entrada").val("");
    $("#salida").val("");
})

$(document).on("click","#convertir",function(event){

    var entrada = $("#entrada").val();
    var salida = $("#salida").val()
    var monEntrada = $("#monedaEntrada").val();
    var monSalida = $("#monedaSalida").val();


    //ERRORES
    //Vacio moneda
    if(monEntrada == "" || monSalida == ""){
        
        // Imprime mensaje de alerta en <p>
        $("#cuidado").html("¿Qué monedas desea convertir?");

        return false;
    }

    // Si la moneda de entrada y salida es la misma
    if(monEntrada == monSalida){
        
        // Imprime mensaje de alerta en <p>
        $("#cuidado").html("Error - Operación invalida");

        return false;
    }

    //Si no se introducen numeros a convertir
    if(entrada == ""){
        
        // Imprime mensaje de alerta en <p>
        $("#cuidado").html("Error 404 - Introduzca un valor");

        return false;
    }

    if(entrada.includes('€') || entrada.includes('$') || entrada.includes('£') || entrada.includes('¥')){

        entrada = entrada.slice(0,-1);

    }

    /*Si se introduce cualquier cosa que no sea un numero 
    en entrada o salida usando el teclado */
    if(!Number(entrada)){
        
    // Imprime mensaje de alerta en <p>
    $("#cuidado").html("Error - Solo se permiten números");

         return false;
    }

    //--------------------------------------------------------//

    //CONVERSION DE MONEDAS
    //Convirtiendo Euros 
    if(monEntrada == "EUR"){
        
        $("#cuidado").html("");

        if(monSalida == "USD"){ // 1€ = 1,17$ 
    
            $("#salida").val(1.17 * entrada + "$");
            $("#entrada").val(entrada + "€");// añadira el simbolo monetario a entrada despues d ela operacion
            return false;
        }
        else if(monSalida == "GBP"){ // 1€ = 0,84£

            $("#salida").val(0.84 * entrada + "£");
            $("#entrada").val(entrada + "€");
            return false;
        }
        else if(monSalida == "JPY"){ // 1€ = 130,75¥

            $("#salida").val(130.75 * entrada + "¥");
            $("#entrada").val(entrada + "€");
            return false;
        }
    }

    //Convirtiendo Dolares
    if(monEntrada =="USD"){

        $("#cuidado").html("");

        if(monSalida == "EUR"){ // 1$ = 0,84€
            
            $("#salida").val(0.84 * entrada + "€");
            $("#entrada").val(entrada + "$");
            return false;
        }
        else if(monSalida == "GBP"){ // 1$ = 0,73£
            
            $("#salida").val(0.73 * entrada + "£");
            $("#entrada").val(entrada + "$");
            return false;
        }
        else if(monSalida == "JPY"){ // 1$ = 110,84¥
            
            $("#salida").val(110.84 * entrada + "¥");
            $("#entrada").val(entrada + "$");
            return false;
        }
    }

    //Convirtiendo Libras
    if(monEntrada =="GBP"){

        $("#cuidado").html("");

        if(monSalida == "EUR"){ // 1£ = 1,14€
            
            $("#salida").val(1.14 * entrada + "€");
            $("#entrada").val(entrada + "£");
            return false;
        }
        else if(monSalida == "USD"){ // 1£ = 1,35$
            
            $("#salida").val(1.35 * entrada + "$");
            $("#entrada").val(entrada + "£");
            return false;
        }
        else if(monSalida == "JPY"){ // 1£ = 149,81¥
            
            $("#salida").val(149.81 * entrada + "¥");
            $("#entrada").val(entrada + "£");
            return false;
        }
    }

    //Convirtiendo Yenes
    if(monEntrada =="JPY"){

        $("#cuidado").html("");

        if(monSalida == "EUR"){ // 1¥ = 0.0076€
            
            $("#salida").val(0.0076 * entrada + "€");
            $("#entrada").val(entrada + "¥");
            return false;
        }
        else if(monSalida == "USD"){ // 1¥ = 0.0090$
            
            $("#salida").val(0.0090 * entrada + "$");
            $("#entrada").val(entrada + "¥");
            return false;
        }
        else if(monSalida == "GBP"){ // 1¥ = 0.0066£
            
            $("#salida").val(0.0066 * entrada + "£");
            $("#entrada").val(entrada + "¥");
            return false;
        }
    }


});