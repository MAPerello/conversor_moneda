
// Mostrar en pantallas
function writeScreen(value){

    var entrada = $("#entrada").val();

    //Si esta vacio imprime
    if(entrada == ''){
        $("#entrada").val(value);
    
    // "Y Si" value es igual a "0" y distinto a "." se corta el flujo con "return false"
    }else if(entrada == '0' && value != '.'){ 
    return false; //Solo permite un cero al inicio

    //si value es igual a "." y entrada contiene un punto, corta el flujo
    } else if (value == '.' && entrada.includes('.')) {

        return false;
    
    /*Si el campo entrada ya contiene un simbolo monetario, borrar el simbolo 
    y concadenar el siguiente numero introducido, para conseguir mayor fluidez*/
    }else if(entrada.includes('€') || entrada.includes('$') || entrada.includes('£') || 
    entrada.includes('¥') || entrada.includes('₩') || entrada.includes('₹') || entrada.includes('₽')){

        entrada = entrada.slice(0,-1);
        entrada += value; //concadena el numero anterior
        $("#entrada").val(entrada); //mostrara los numeros en la pantalla
    
    }else{
        //Si no esta vacio concadena
        entrada += value; //concadena el numero anterior
        $("#entrada").val(entrada); //mostrara los numeros en la pantalla
    }
    
}

//Borrar todo
$(document).on("click","#c",function(event){

    $("#entrada").val("");
    $("#salida").val("");
})

$(document).on("click","#convertir",function(event){

    var entrada = $("#entrada").val();
    var salida = $("#salida").val()
    var monEntrada = $("#monedaEntrada").val();
    var monSalida = $("#monedaSalida").val();

    //Coge el texto contenido dentro de "option" del "select"
    var signoEntrada = $('select[name="monedaEntrada"] option:selected').text();
    var signoSalida = $('select[name="monedaSalida"] option:selected').text();

    //ERRORES
    //Vacio moneda
    if(monEntrada == "" || monSalida == ""){
        
        // Imprime mensaje de alerta en <p>
        $("#cuidado").html("¿Qué monedas desea convertir?");

        return false;
    }

    // Si la moneda de entrada y salida es la misma
    if(monEntrada == monSalida){
        
        // Imprime mensaje de alerta en <p>
        $("#cuidado").html("Error - Operación invalida");

        return false;
    }

    //Si no se introducen numeros a convertir
    if(entrada == ""){
        
        // Imprime mensaje de alerta en <p>
        $("#cuidado").html("Error 404 - Introduzca un valor");

        return false;
    }

    if(entrada.includes('€') || entrada.includes('$') || entrada.includes('£') || 
    entrada.includes('¥') || entrada.includes('₩') || entrada.includes('₹') || entrada.includes('₽')){

        entrada = entrada.slice(0,-1);

    }

    /*Si se introduce cualquier cosa que no sea un numero 
    en entrada o salida usando el teclado */
    if(!Number(entrada)){
        
        // Imprime mensaje de alerta en <p>
        $("#cuidado").html("Error - Solo se permiten números");

         return false;
    }

    //--------------------------------------------------------//

    //CONVERSION DE MONEDAS
    
    numero = (entrada/monSalida)*monEntrada;
    $("#salida").val(numero.toFixed(2) + signoSalida);
    $("#entrada").val(entrada + signoEntrada);
    $("#cuidado").html("");
    
    //-------EXPLICACION DE OPERACION--------//

    /* Se toma como valor capital al euro, 
    1€ = 1€ / 1$ = 0.84$ / 1£ = 1.14€ / 1¥ = 0.0076€ / 
    1₩ = 0.00079€ / 1₹ = 0.0125€ / 1₽ = 0.0139€ */
    
    /* Se toma el valor de entrada, se divide por la 
    moneda de salida y se multiplica por la moneda de entrada
    y se guarda el valor en "numero" */

    /* Se le aplica .toFixed(2) a "numero", 
    que limita el numero de deciamles a 2 y despues se le suma el signo 
    del texto del select de salida */

    /* Al terminar las operaciones se coge el valor de entrada y 
    se le concadena el texto del select de entrada */

    /* Finalmente se borra cualquier mensaje de 
    advertencia del campo "cuidado" */


});